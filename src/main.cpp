// Copyright (c) 2021 Helmholtz-Zentrum Dresden - Rossendorf e.V.
//
// SPDX-License-Identifier: MIT

#include <iostream>

#include "src/helloworld.h"

int main() {
    std::string gitlabUserName = getUserNameViaEnv();
    std::cout << "Hello " << gitlabUserName << "!" << std::endl;
    return 0;
}
